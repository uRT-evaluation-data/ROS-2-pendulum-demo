RTI Connext DDS Non-commercial license is for academic, research, evaluation and personal use only. USE FOR COMMERCIAL PURPOSES IS PROHIBITED. See RTI_LICENSE.TXT for terms. Download free tools at rti.com/ncl. License issued to Non-Commercial User license@rti.com For non-production use only.
Expires on 00-jan-00 See www.rti.com for more information.
Initial major pagefaults: 2
Initial minor pagefaults: 292851
PRESWriterHistoryDriver_completeBeAsynchPub:!make_sample_reclaimable in topic 'rt/pendulum_command'
PRESWriterHistoryDriver_completeBeAsynchPub:!make_sample_reclaimable in topic 'rt/pendulum_sensor'
rttest statistics for /home/thomas/Desktop/ros2_pendulum_benchmark//2022-08-08_16:38:23/demo/0_stress/fifo/results.csv:
  - Minor pagefaults: 0
  - Major pagefaults: 0
  Latency (time after deadline was missed):
    - Min: 2916 ns
    - Max: 46199 ns
    - Mean: 4881.541000 ns
    - Standard deviation: 1744.330809


PendulumMotor received 996 messages
PendulumController received 989 messages
