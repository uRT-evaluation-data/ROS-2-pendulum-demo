RTI Connext DDS Non-commercial license is for academic, research, evaluation and personal use only. USE FOR COMMERCIAL PURPOSES IS PROHIBITED. See RTI_LICENSE.TXT for terms. Download free tools at rti.com/ncl. License issued to Non-Commercial User license@rti.com For non-production use only.
Expires on 00-jan-00 See www.rti.com for more information.
Initial major pagefaults: 2
Initial minor pagefaults: 292846
rttest statistics for /home/thomas/Desktop/ros2_pendulum_benchmark//2022-08-08_16:38:23/demo/1_stress/fifo/results.csv:
  - Minor pagefaults: 0
  - Major pagefaults: 0
  Latency (time after deadline was missed):
    - Min: 2343 ns
    - Max: 20105 ns
    - Mean: 2763.589000 ns
    - Standard deviation: 557.647025


PendulumMotor received 997 messages
PendulumController received 998 messages
