# ROS 2 Real-Time Benchmarks

Contents of this directory:

- Benchmark result data of the ROS 2 "pendulum demo" benchmark application (Humble Hawksbill) for various system configurations:
  - [2C4T](2C4T/):<br>
    2 cores, 4 threads running Ubuntu 22.04 with generic Linux kernel 5.15 and dynamic frequency scaling enabled (400 MHz - 4 GHz).
  - [2C2T](2C2T/):<br>
    2 cores, 2 threads running Ubuntu 22.04 with generic Linux kernel 5.15 and fixed CPU frequency (3.5 GHz).
  - [2C4T_RT](2C4T_RT/):<br>
    2 cores, 4 threads running Ubuntu 22.04 with real-time  Linux kernel 5.15 and dynamic frequency scaling enabled (400 MHz - 4 GHz).
  - [2C2T_RT](2C2T_RT/):<br>
    2 cores, 2 threads running Ubuntu 22.04 with real-time Linux kernel 5.15 and fixed CPU frequency (3.5 GHz).
- [`LICENSE`](LICENSE) file:<br>
  Data may be accessed, modified and republished according to the Creative Commons Attribution 4.0 International license (CC-BY).
